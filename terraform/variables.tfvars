resource_group_name = "application-rg"
location            = "East US"

virtual_network = {
  name = "hieutt65-webapp-vnet"

  cidr_blocks = ["10.0.0.0/16"]
  subnets = {
    "subnet-1" = {
      cidr_blocks         = ["10.0.0.0/24"]
      security_group_name = "nsg-2"
    },
    "subnet-2" = {
      cidr_blocks         = ["10.0.1.0/24"]
      security_group_name = "nsg-1"
    }
  }
}

nework_security_groups = {
  "nsg-1" = {
    security_rules = {
      "allowVmInSameSubnet_Inbound" = {
        access                     = "Allow"
        direction                  = "Inbound"
        priority                   = 300
        protocol                   = "Tcp"
        source_address_prefix      = "10.0.0.0/24"
        source_port_range          = "*"
        destination_address_prefix = "*"
        destination_port_range     = "*"
      }
      "allowVmInSameSubnet_Outbound" = {
        access                     = "Allow"
        direction                  = "Outbound"
        priority                   = 300
        protocol                   = "Tcp"
        source_address_prefix      = "10.0.0.0/24"
        source_port_range          = "*"
        destination_address_prefix = "*"
        destination_port_range     = "*"
      }
      "denyInternetAccess" = {
        access                     = "Deny"
        direction                  = "Inbound"
        priority                   = 4096
        protocol                   = "Tcp"
        source_address_prefix      = "Internet"
        source_port_range          = "*"
        destination_address_prefix = "*"
        destination_port_range     = "*"
      }
    }
  }
  "nsg-2" = {
    security_rules = {
      "allowSshFromEveryone_Inbound" = {
        access                     = "Allow"
        direction                  = "Inbound"
        priority                   = 300
        protocol                   = "Tcp"
        source_address_prefix      = "*"
        source_port_range          = "*"
        destination_address_prefix = "10.0.0.0/24"
        destination_port_range     = "22"
      }
      "allow80FromEveryone_Inbound" = {
        access                     = "Allow"
        direction                  = "Inbound"
        priority                   = 400
        protocol                   = "Tcp"
        source_address_prefix      = "*"
        source_port_range          = "*"
        destination_address_prefix = "10.0.0.0/24"
        destination_port_range     = "80"
      }
    }
  }
}

virtual_machine = {
  name = "hieutt65-linux-virtual-machine"
  source_image_ref = {
    image_name          = "hieutt65-ubuntu22.04-packer"
    resource_group_name = "packer-group"
  }

  admin_username = "ubuntu"

  allocate_public_ipv4_address = true

  size = "Standard_B1s"

  os_disk = {
    caching              = "ReadWrite"
    disk_size            = 32
    storage_account_type = "Standard_LRS"
  }
}