variable "resource_group_name" {
  type        = string
  description = "Name of the resource group"
}

variable "location" {
  type        = string
  description = "Location of the resource group. Default to East US"
  default     = "East US"
}

variable "virtual_network" {
  type = object({
    name        = string
    cidr_blocks = list(string)
    subnets = map(object({
      cidr_blocks         = list(string)
      security_group_name = string
    }))
  })
}

variable "nework_security_groups" {
  type = map(object({
    security_rules = map(object({
      access                     = string
      direction                  = string
      priority                   = number
      protocol                   = string
      source_port_range          = string
      source_address_prefix      = string
      destination_port_range     = string
      destination_address_prefix = string
    }))
  }))
}

variable "virtual_machine" {
  type = object({
    name = string
    source_image_ref = object({
      resource_group_name = string
      image_name          = string
    })
    admin_username               = string
    allocate_public_ipv4_address = bool
    size                         = string
    os_disk = object({
      caching              = string
      storage_account_type = string
      disk_size            = number
    })
  })
}

variable "virtual_machine_public_ssh_key" {
  type = string
}