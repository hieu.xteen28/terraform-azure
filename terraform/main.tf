module "webapp" {
  source = "./modules/webapp"

  resource_group_name = var.resource_group_name
  location            = var.location

  virtual_network        = var.virtual_network
  nework_security_groups = var.nework_security_groups
  virtual_machine        = var.virtual_machine
  virtual_machine_public_ssh_key = var.virtual_machine_public_ssh_key
}