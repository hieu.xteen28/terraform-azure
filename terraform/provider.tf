terraform {
  backend "azurerm" {
    resource_group_name  = "packer-group"
    storage_account_name = "hieutt65storageaccount"
    container_name       = "tfstate"
    key                  = "terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}