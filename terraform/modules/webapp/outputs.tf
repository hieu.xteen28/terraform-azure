output "resource_group" {
  value = azurerm_resource_group.rg
}

output "nsg" {
  value = module.nsg
}

output "vnet" {
  value = module.vnet
}