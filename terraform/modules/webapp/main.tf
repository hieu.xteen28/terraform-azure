resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
  location = var.location
}

module "nsg" {
  source   = "../common/nsg"
  for_each = var.nework_security_groups

  name                = each.key
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  security_rules = each.value.security_rules
}

resource "azurerm_subnet_network_security_group_association" "nsg_assocation" {
  for_each = var.virtual_network.subnets

  subnet_id                 = module.vnet.subnets[each.key].id
  network_security_group_id = module.nsg[each.value.security_group_name].nsg.id
}

module "vnet" {
  source = "../common/vnet"

  name                = var.virtual_network.name
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  cidr_blocks = var.virtual_network.cidr_blocks
  subnets     = var.virtual_network.subnets
}

module "virtual_machine" {
  source = "../common/linux_vm"

  name                = var.virtual_machine.name
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  source_image_id = data.azurerm_image.search.id

  admin_username = var.virtual_machine.admin_username
  ssh_public_key = file(var.virtual_machine_public_ssh_key)

  network_interface_ids = [azurerm_network_interface.unic.id]

  size = var.virtual_machine.size

  os_disk = {
    caching              = var.virtual_machine.os_disk.caching
    storage_account_type = var.virtual_machine.os_disk.storage_account_type
    size                 = var.virtual_machine.os_disk.disk_size
  }
}

data "azurerm_image" "search" {
  name                = var.virtual_machine.source_image_ref.image_name
  resource_group_name = var.virtual_machine.source_image_ref.resource_group_name
}

resource "azurerm_network_interface" "unic" {
  name                = "AZNIC"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = module.vnet.subnets["subnet-1"].id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = var.virtual_machine.allocate_public_ipv4_address == true ? azurerm_public_ip.public_ip[0].id : null
  }
}

resource "azurerm_public_ip" "public_ip" {
  count = var.virtual_machine.allocate_public_ipv4_address == true ? 1 : 0

  name                = "AzurePublicIp"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Dynamic"
}