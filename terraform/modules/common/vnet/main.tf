resource "azurerm_virtual_network" "vnet" {
  name                = var.name
  resource_group_name = var.resource_group_name
  address_space       = var.cidr_blocks
  location            = var.location
}

resource "azurerm_subnet" "subnets" {
  for_each = var.subnets

  name                = each.key
  resource_group_name = var.resource_group_name

  address_prefixes     = each.value.cidr_blocks
  virtual_network_name = azurerm_virtual_network.vnet.name
}