variable "name" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "cidr_blocks" {
  type = list(string)
}

variable "location" {
  type = string
}

variable "subnets" {
  type = map(object({
    cidr_blocks = list(string)
  }))
  default = {}
}