variable "service_plan_name" {
  type = string
}

variable "pricing_plan" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "web_app_name" {
  type = sring
}