variable "name" {
  type        = string
  description = "Name of the Linux machine"
}

variable "resource_group_name" {
  type        = string
  description = "Name of resource group that the machine belongs to"
}

variable "location" {
  type = string
}

variable "admin_username" {
  type        = string
  description = "Admin's username of the Linux machine"
}

variable "ssh_public_key" {
  type        = string
  description = "Public key to SSH to the machine"
}

variable "network_interface_ids" {
  type        = list(string)
  description = "List of network interface's ID attached to the machine"
}

variable "size" {
  type        = string
  description = "Size of the machine"
}

variable "os_disk" {
  type = object({
    size                 = number
    storage_account_type = string
    caching              = string
  })
}

variable "source_image_id" {
  type        = string
  description = "ID of the base image"
}