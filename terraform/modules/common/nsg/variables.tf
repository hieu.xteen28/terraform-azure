variable "name" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "security_rules" {
  type = map(object({
    access   = string
    priority = number

    direction = string
    protocol  = string

    source_port_range     = string
    source_address_prefix = string

    destination_port_range     = string
    destination_address_prefix = string
  }))
  default = {}
}