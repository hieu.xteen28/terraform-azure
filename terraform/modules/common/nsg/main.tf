resource "azurerm_network_security_group" "nsg" {
  name                = var.name
  resource_group_name = var.resource_group_name
  location            = var.location

  dynamic "security_rule" {
    for_each = var.security_rules

    content {
      name      = security_rule.key
      direction = security_rule.value.direction

      access   = security_rule.value.access
      priority = security_rule.value.priority

      protocol = security_rule.value.protocol

      source_port_range     = security_rule.value.source_port_range
      source_address_prefix = security_rule.value.source_address_prefix

      destination_port_range     = security_rule.value.destination_port_range
      destination_address_prefix = security_rule.value.destination_address_prefix
    }
  }
}